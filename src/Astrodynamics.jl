module Astrodynamics
    export GaussJackson, iers2010, cTime, cDist, solveRK, RK4, RK8_7_13, State, KeplerElements, keplerToCart, orbit_plotter, orbit_plotter3d, getStateByTime
    using Plots
    using Makie
    using GLMakie
    using Base
    # using WGLMakie
    # using CairoMakie
    include("plotter.jl")
    include("AdamsBashforthCoeff.jl")
    include("BinomialCoeff.jl")
    include("iers_2010.jl")
    include("constants.jl")
    include("AdamsMoultonCoeff.jl")
    include("StoemerCowellCoef.jl")
    include("Structures.jl")
    include("RKSolver.jl")
    include("RK4.jl")
    include("RK8(7)-13 Coefficients.jl")
    include("RK8(7)-13.jl")
    include("Gauss Jackson.jl")
    include("Kepler Problem.jl")
    # Write your package code here.
end
