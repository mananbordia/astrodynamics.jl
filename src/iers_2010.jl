"""
iers2010 contains constants defined in the IERS 2010 conventions
"""
module iers2010
    struct iersConst
        val :: Float64
        unit :: String
        uncertainity :: Float64
    end
    astronomical_unit = iersConst(1.49597870700e+11,"m",-1)
    earth_radius_equator = iersConst(6.3781366000e+06, "m",-1)
    earth_gm = iersConst(3.9860044180e+14,"m^3/s^2",-1)
    earth_j2 = iersConst(1.0826359e-3,"",1e-10)
    earth_rotation_rate = iersConst(7.2921150000e-05,"rad/s",-1)
    earth_mean_equatorial_gravity = iersConst(9.7803278000e+00,"m/s^2",1e-6)
    earth_density_sea_water= iersConst(1.0250000000e+03,"kg/m^3",-1)
    big_g = iersConst(6.67428e-11, "m^3/(kg*s^2)",6.7e-15)
    speed_of_light = iersConst(299792458,"m/s",0)
    sun_gm = iersConst(1.32712442099e20,"m^3/s^2",1e10)
end
