### Constants
GM = iers2010.earth_gm.val
hour = cTime.hour
km = cDist.km

### Function to get Eccentric Anomaly
# Function getE : takes M (Mean Anomaly), tol (tolerance), e (eccentricity)
# Outputs : Eccentric Anomaly (In MKS)


function getEccentricAnomaly(;kepEle , limitItr = 100)
    E = kepEle.M
    e = kepEle.e
    M = kepEle.M

    tolerance = 10.0^(-10)
    numItr = 0
    while(numItr<limitItr && abs(E - e*sin(E)- M)>tolerance)
        E = e*sin(E)+M
        numItr += 1
    end

    if numItr==limitItr
        error("E is not convergent under given iteration limits")
    end
    return eccAnomaly = E
end


### Function to get Postion and Velocity in Perifocal Frame
# Function getPeri : takes a (Semi-Major Axis), E (Eccentric Anomaly), e (eccentricity), n (Mean Motion)
# Return : Position-X, Position-Y, Velocity-X, Velocity-Y (all in Perifocal Frame)
# Edot = dE/dt

function getPeriCart(;kepEle ,meanMotion, eccAnomaly)
    state = FState()
    a = kepEle.a
    e = kepEle.e
    E = eccAnomaly
    n = meanMotion

    Edot = n*a/(1-e*cos(E))
    state.position.perifocal.x = a*(cos(E)-e)
    state.position.perifocal.y = a*(sqrt(1-e^2))*sin(E)
    state.velocity.perifocal.x = -a*sin(E)*Edot
    state.velocity.perifocal.y = a*(sqrt(1-e^2))*cos(E)*Edot

    return state
end

### Function to get Postion and Velocity in Perifocal Frame
# Function kep2cart : takes all kepler elments i.e -
# a (Semi-Major Axis), E (Eccentric Anomaly), e (eccentricity), M (Mean Anomaly), Ω (Angle from vernal equinox), ω (Angle of Perigee), I (Angle of Inclination)
# Return : Position-X, Position-Y, Velocity-X, Velocity-Y (all in Perifocal Frame)

function getPeriFrame(;kepEle, runTime)
    States = Array{FState,1}()
    meanMotion = sqrt(GM/(kepEle.a^3))

    for time = 1:runTime+1
        E = getEccentricAnomaly(kepEle=kepEle)
        state = getPeriCart(kepEle = kepEle, meanMotion = meanMotion, eccAnomaly = E)
        push!(States,state)
        kepEle.M += meanMotion
    end

    return States
end

### Rotation Matrices
# Rotation Matrix
function Rotx(ang)
    return Array(transpose([[1,0,0] [0,cos(ang),sin(ang)] [0,-sin(ang),cos(ang)]]))
end

function Roty(ang)
    return Array(transpose([[cos(ang),0,-sin(ang)] [0,1,0] [sin(ang),0,cos(ang)]]))
end

function Rotz(ang)
    return Array(transpose([[cos(ang),sin(ang),0] [-sin(ang),cos(ang),0] [0,0,1]]))
end

### Function to extract Velocity and Position Matrices in Perifocal Frame from States

function getPeriMatrices(;States)
    Vx = []
    Vy = []
    Vz = []
    Rz = []
    Ry = []
    Rx = []
    for state in States
        push!(Rx,state.position.perifocal.x)
        push!(Ry,state.position.perifocal.y)
        push!(Rz,state.position.perifocal.z)
        push!(Vx,state.velocity.perifocal.x)
        push!(Vy,state.velocity.perifocal.y)
        push!(Vz,state.velocity.perifocal.z)
    end
    PosMat = cat(Rx, Ry, Rz, dims=2)
    VelMat = cat(Vx,Vy,Vz, dims=2)
    return Array(transpose(PosMat)), Array(transpose(VelMat))
end

### Function to extract Velocity and Position Matrices in Inertial Frame from States
function getInertialMatrices(;States)
    Vx = []
    Vy = []
    Vz = []
    Rz = []
    Ry = []
    Rx = []
    for state in States
        push!(Rx,state.position.inertial.x)
        push!(Ry,state.position.inertial.y)
        push!(Rz,state.position.inertial.z)
        push!(Vx,state.velocity.inertial.x)
        push!(Vy,state.velocity.inertial.y)
        push!(Vz,state.velocity.inertial.z)
    end
    PosMat = cat(Rx, Ry, Rz, dims=2)
    VelMat = cat(Vx,Vy,Vz, dims=2)
    return Array(transpose(PosMat)), Array(transpose(VelMat))
end

# Author : Manan Bordia
# Last Update - 02 September 2020
function getEarthCenteredMatrices(;States)
    Vx = []
    Vy = []
    Vz = []
    Rz = []
    Ry = []
    Rx = []
    for state in States
        push!(Rx,state.position.earth.x)
        push!(Ry,state.position.earth.y)
        push!(Rz,state.position.earth.z)
        push!(Vx,state.velocity.earth.x)
        push!(Vy,state.velocity.earth.y)
        push!(Vz,state.velocity.earth.z)
    end
    PosMat = cat(Rx, Ry, Rz, dims=2)
    VelMat = cat(Vx,Vy,Vz, dims=2)
    return Array(transpose(PosMat)), Array(transpose(VelMat))
end

### Function to transform Velocity and Position in Perifocal frame to Inertial frame
function getInertFrame(;kepEle, States)
    Ω = kepEle.Ω
    I = kepEle.I
    ω = kepEle.ω
    Rperi, Vperi = getPeriMatrices(States = States)
    Rgeo = Rotz(-Ω)*Roty(-I)*Rotz(-ω)*Rperi
    Vgeo = Rotz(-Ω)*Roty(-I)*Rotz(-ω)*Vperi
    return Rgeo, Vgeo
end

### Function to Add Inertial Frame in States variable
# Note : This function contains some integer division manipulations.
function updateInertFrame(;States, R, V)
    for (index, val) in enumerate(R)
        if(index%3==0)
            States[div(index-1,3)+1].position.inertial.x = val
        elseif(index%3==1)
            States[div(index-1,3)+1].position.inertial.y = val
        else
            States[div(index-1,3)+1].position.inertial.z = val
        end
    end
    for (index, val) in enumerate(V)
        if(index%3==0)
            States[div(index-1,3)+1].velocity.inertial.x = val
        elseif(index%3==1)
            States[div(index-1,3)+1].velocity.inertial.y = val
        else
            States[div(index-1,3)+1].velocity.inertial.z = val
        end
    end
    return States
end

### Function to get States in Cartesian Coordiantes from Kepler elements

function getEarthFrame(;States)
    for (index, val) in enumerate(States)
        Ri = [val.position.inertial.x,val.position.inertial.y,val.position.inertial.z]
        Vi = [val.velocity.inertial.x,val.velocity.inertial.y,val.velocity.inertial.z]

        θ = (280.4606+360.9856473*index/(24*hour))*(π/180)
        Ref = Rotz(θ)*Ri
        Vef = Rotz(θ)*Vi
        States[index].position.earth.x = Ref[1]
        States[index].position.earth.y = Ref[2]
        States[index].position.earth.z = Ref[3]

        States[index].velocity.earth.x = Vef[1]
        States[index].velocity.earth.y = Vef[2]
        States[index].velocity.earth.z = Vef[3]

    end
    return States
end

function keplerToCart(;kepEle, runTime)
    States = getPeriFrame(kepEle = kepEle, runTime = runTime)
    R, V = getInertFrame(kepEle = kepEle, States = States)
    States = updateInertFrame(States = States, R = R, V = V)
    States = getEarthFrame(States = States)
   return States
end
