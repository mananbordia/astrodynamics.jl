# Default solver is Runge Kutta 4th Order

"""
solveRK(df, y0, t; h = nothing, RK = RK4)

This function solves differential equation using Runge-Kutta Method

    INPUT :
        Compulsory arguments :
            df - Function df(t)/dt
            y0 - Matrix containing initial values
            t - Time span

        Optional argument :
            h - Time step (default = nothing)
            RK - Type of Runge Kutta Method (default = RK4)

    OUTPUT :
        Returns labeled tuple (y,t), where
        y - Matrix containing the integrated function i.e f(t)
        t - Vector containing time at each step

# Example 1 - Testing function : (Single Integration) ``dy/dt = y, y(0) = 1``, Solution ``y = e^t``
```julia-repl
julia> function df(y, t)
           dz = zeros(size(y))
           dz[1,:] = y[1,:]
           return dz
       end
df (generic function with 1 method)

julia> t = (0, 1)
(0, 1)

julia> y0 = [1]
1-element Vector{Int64}:
 1

julia> sol1 = solveRK(df, y0, t)
(y = [1.0; 1.0100501670833333; … ; 2.69123447212908; 2.7182818282344035], t = [0.0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09  …  0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99, 1.0])

julia> sol1.y
101×1 Matrix{Float64}:
 1.0
 1.0100501670833333
 1.0202013400250696
 1.030454533950962
 ⋮
 2.63794445914269
 2.6644562417136277
 2.69123447212908
 2.7182818282344035

julia> sol1.t
101-element Vector{Float64}:
 0.0
 0.01
 0.02
 0.03
 ⋮
 0.97
 0.98
 0.99
 1.0

# You can also set your own RK method and set time step
julia> sol2 = solveRK(df, y0, t, RK = RK8_7_13)
(y = [1.0; 1.010050167084168; … ; 2.6912344723492647; 2.7182818284590473], t = [0.0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09  …  0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99, 1.0])

julia> sol3 = solveRK(df, y0, t, h = 0.2, RK = RK8_7_13)
(y = [1.0; 1.2214027581607014; … ; 2.2255409284963434; 2.718281828464963], t = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0])
```

# Example 2 - Testing function : (Double Integration) ``d^2y/dt^2 = 6t, y(0) = 0``, Solution  ``y = t^3``
```julia-repl
julia> function df(y, t)
           dz = zeros(size(y))
           dz[1] = y[2]
           dz[2] = 6*t
           return dz
       end
df (generic function with 1 method)

julia> t = (0,2)
(0, 2)

julia> y0 = [0 ; 0]
2-element Vector{Int64}:
 0
 0

julia> sol = solveRK(df, y0, t, RK = RK8_7_13)
(y = [0.0 0.0; 8.000000000000015e-6 0.0012000000000000008; … ; 7.762391999999996 11.761200000000015; 7.999999999999996 12.000000000000014], t = [0.0, 0.02, 0.04, 0.06, 0.08, 0.1, 0.12, 0.14, 0.16, 0.18  …  1.82, 1.84, 1.86, 1.88, 1.9, 1.92, 1.94, 1.96, 1.98, 2.0])
```
# Examples 3 - Testing function : (Double Integration) Two body Problem
```julia-repl
julia> GM = iers2010.earth_gm.val
3.986004418e14

julia> A = 42158.98*1000
4.215898e7

julia> e = 0.0012778
0.0012778

julia> meanMotion = sqrt(GM/(A^3))
7.29346235057309e-5

julia> function getA(r)
           rx, ry = r
           ax = -GM*(rx)/((rx^2 + ry^2)^(3/2))
           ay = -GM*(ry)/((rx^2 + ry^2)^(3/2))
           a = [ax  ay]
           return a
       end
getA (generic function with 1 method)

julia> function df(y, t)
           # [vx vy ; ax ay]
           dz = zeros(size(y))
           dz[1,:] = y[2,:]
           dz[2,:] = getA(y[1,:])
           return dz
       end
df (generic function with 1 method)

julia> t = (0,24*3600)
(0, 86400)

julia> y0 = [A*(1-e) 0 ; 0 A*(sqrt((1+e)/(1-e)))*meanMotion]
2×2 Matrix{Float64}:
 4.21051e7     0.0
 0.0        3078.78

julia> sol1 = solveRK(df, y0, t, RK = RK8_7_13)
(y = [4.2105109255356e7 0.0; 4.202121719813388e7 -194.1298172261963; … ; 4.206298538306282e7 137.59547928294307; 4.209798080232454e7 -56.61457879190887]

[0.0 3078.780889640156; 2.658299778016995e6 3072.6466078497137; … ; -1.8841493471764945e6 3075.7007407095834; 775245.0076105833 3078.2596479042104], t = [0.0, 864.0, 1728.0, 2592.0, 3456.0, 4320.0, 5184.0, 6048.0, 6912.0, 7776.0  …  78624.0, 79488.0, 80352.0, 81216.0, 82080.0, 82944.0, 83808.0, 84672.0, 85536.0, 86400.0])
```
"""
function solveRK(df, y0, t; h = nothing, RK = RK4)
    # y0 = [ax ay az ; bx by bz ; cx cy cz]
    # t = (a,b)
    # df should be of form (Y :: matrix , time :: float)

    # Is no value for h is provided
    if(h==nothing)
        h = (t[2] - t[1])/100
    end

    # Time vector
    T = collect(t[1]:h:t[2])

    # number of steps
    n = size(T)[1]

    # number of parameters in y0
    m = (size(y0)...,1)

    # Matrix y
    Y = zeros(n,m[1],m[2])
    Y[1,:,:] = y0

    # Integrating using Runge Kutta Method --- Change this to any method of your choice
    for i = 2 : n
    #     apply Runge Kutta Method
        Y[i,:,:] = RK(df,Y[i-1,:,:],T[i-1],h)
    end

    # Will return a named tupple so that user can access it by dot notation
    if size(Y)[3] == 1
        return (y = Y[:,:,1], t = T)
    elseif size(Y)[2] == 1
        return (y = Y[:,1,:], t = T)
    else
        return (y = Y, t = T)
    end
end
