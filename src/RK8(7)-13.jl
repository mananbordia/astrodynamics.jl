"""
RK8_7_13(df,yi,ti,h)

Predicts the value of function at next step using Runge Kutta 8(7)-13 by Prince-Dormand

    INPUT :
        Compulsory arguments :
            df - Function df(t)/dt
            yi - Matrix containing the intital values of function
            t - Initial time
            h - Time step

    OUTPUT :
        Returns a Matrix ϕ - Contains the values of function at next step
"""
function RK8_7_13(df,yi,ti,h)
    K = zeros(13,size(yi)[1],size(yi)[2])
    for i = 1 : 13
        yTemp = copy(yi);
        tTemp = ti + c[i]*h;
        for j = 1:i-1
           yTemp +=  a[i,j]*K[j,:,:];
        end
        K[i,:,:] = h*df(yTemp, tTemp);
    end
    ϕ = copy(yi)
    for i = 1:13
        ϕ += bs[i]*K[i,:,:];
    end
    return ϕ
end
