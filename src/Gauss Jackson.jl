# This is the del operator
function ∇(power,i,a,z1,z2,q,dp,start)
    #power means how many times operator is appplied
    #i = index
    # a = variable (accleration) array on which ∇ is applied

    if(i<=0)
        throw("Trying to excess index i <= 0")
    end

    if(get(dp,(power,i),-12345)!= -12345 && i < start)
        return dp[(power,i)]
    elseif(power<0)
        if(power<-2)
            throw("Can't find for power < -2 of ∇ operator")
        end
        dp[(power,i)] = getDelNeg(power,i,a,z1,z2,q,dp,start)

    elseif(power==0)
        dp[(power,i)] = a[i,:]
    else
        dp[(power,i)] = ∇(power-1,i,a,z1,z2,q,dp,start) - ∇(power-1,i-1,a,z1,z2,q,dp,start)
    end

    return dp[(power,i)]
end

function getDelNeg(power,i,a,z1,z2,q,dp,start)
    if(power==-1)
        if(i==2*q+1)
            return z1
        end
        return a[i,:] + ∇(-1,i-1,a,z1,z2,q,dp,start)
    end
    if(power==-2)
        if(i==2*q+1)
            return z2
        end
        return ∇(-1,i,a,z1,z2,q,dp,start) + ∇(-2,i-1,a,z1,z2,q,dp,start)
    end
end

"""
GaussJackson(t, y0 ; h = nothing)

Predicts the orbit path for a satellite revolving around earth.

Uses Partial 18th order Gauss Jackson Integrator to integrate acceleration over a time span and returns States

    INPUT :
        Compulsory arguments
            t - Time span
            y - Matrix containing initial values of position and velocity

        Optional arguments
            h - Time step

    OUTPUT :
        Returns States - Vector containing State for each time step

# Example - Testing : INSAT 1A Orbit Determination
```julia-repl
julia> GM = iers2010.earth_gm.val
3.986004418e14

julia> A = 42158.98*1000
4.215898e7

julia> e = 0.0012778
0.0012778

julia> t = (0, 24*cTime.hour)
(0, 86400)

julia> meanMotion = sqrt(GM/(A^3))
7.29346235057309e-5

julia> y0 = [A*(1-e) 0 ; 0 A*(sqrt((1+e)/(1-e)))*meanMotion]
2×2 Matrix{Float64}:
 4.21051e7     0.0
 0.0        3078.78

julia> tmp = GaussJackson(t,y0,h=1*cTime.hour)
25-element Vector{State}:
 State(Astrodynamics.Velocity(0.0, 0.0, 0.0), Astrodynamics.Position(4.2105109255356e7, 0.0, 0.0), Astrodynamics.Acceleration(-0.22483720534310206, -0.0, 0.0), 0.0)
 State(Astrodynamics.Velocity(0.0, 0.0, 0.0), Astrodynamics.Position(4.065657876369777e7, 1.0956217390206119e7, 0.0), Astrodynamics.Acceleration(-0.217073564018384, -0.05849742475566653, 0.0), 3600.0)
 State(Astrodynamics.Velocity(0.0, 0.0, 0.0), Astrodynamics.Position(3.641101009867229e7, 2.1158717566123288e7, 0.0), Astrodynamics.Acceleration(-0.19433050677917785, -0.11292694974073694, 0.0), 7200.0)
 State(Astrodynamics.Velocity(0.0, 0.0, 0.0), Astrodynamics.Position(2.9661416750624787e7, 2.9906183711996287e7, 0.0), Astrodynamics.Acceleration(-0.15820978602284957, -0.15951533824611727, 0.0), 10800.0)
 ⋮
 State(Astrodynamics.Velocity(0.0, 0.0, 0.0), Astrodynamics.Position(3.020612630760489e7, -2.935491042203275e7, 0.0), Astrodynamics.Acceleration(-0.16112317692706626, 0.15658268714901621, 0.0), 75600.0)
 State(Astrodynamics.Velocity(0.0, 0.0, 0.0), Astrodynamics.Position(3.679386143258642e7, -2.0484715413656473e7, 0.0), Astrodynamics.Acceleration(-0.1963806764466376, 0.1093335168726796, 0.0), 79200.0)
 State(Astrodynamics.Velocity(0.0, 0.0, 0.0), Astrodynamics.Position(4.085115693101489e7, -1.020578858386708e7, 0.0), Astrodynamics.Acceleration(-0.21811632206785625, 0.05449170199694268, 0.0), 82800.0)
 State(Astrodynamics.Velocity(0.0, 0.0, 0.0), Astrodynamics.Position(4.209798128329443e7, 775244.7811917273, 0.0), Astrodynamics.Acceleration(-0.22479898913242716, -0.004139729218114468, 0.0), 86400.0)

julia> using Plots

julia> plot([i.position.x for i in tmp], [i.position.y for i in tmp])
```
"""
function GaussJackson(t, y0 ; h = -12345)

    # Using Dynamic Programming to reduce time from exponential to polynomial
    dp = Dict()

    # Gravitational Constants
    GM = iers2010.earth_gm.val
    # Accleration function
    function getA(r)
        rx, ry = r
        ax = -GM*(rx)/((rx^2 + ry^2)^(3/2))
        ay = -GM*(ry)/((rx^2 + ry^2)^(3/2))
        acc = [ax  ay]
        return acc
    end

    # dy/dt function for RK integration
    function df(y, t)
        # [vx vy ; ax ay]
        dz = zeros(size(y))
        dz[1,:] = y[2,:]
        dz[2,:] = getA(y[1,:])
        return dz
    end


    if(h==-12345)
        h = (t[2] - t[1])/100
    end


    # Order must be even
    order = 18
    @assert(order%2==0)

    # 1 -> shifted by order/2 as order/2 values will be added from negative time
    # So the transformation goes like this ith index -> (order/2+i)^ith index
    # Lets define q = order/2, so that we can use q + i everywhere to remove confusion

    # Offset
    q = div(order,2)

    # Time in back direction
    trev = (t[1], t[1]-q*(h))

    # Applying RK integration in reverse direction
    solrev = solveRK(df, y0, trev; h = -h, RK = RK8_7_13)
    reverse!(solrev.t)
    reverse!(solrev.y, dims = 1)
    # display(solrev.y)

    # Time in back direction
    tforw = (t[1], t[1] + (q)*h)

    # Applying RK integration in reverse direction
    solforw = solveRK(df, y0, tforw; h = h, RK = RK8_7_13)
    # display(solforw.y)

    # Time vector
    T = collect(t[1]-q*(h): h : t[2])

    # number of steps
    n = size(T)[1]

    # number of parameters in y0
    m = (size(y0)...,1)

    # Matrix y
    Y = zeros(n,m[1],m[2])

    # Matrix A - accleration matrix
    Acc = zeros(n,m[2])

    # Merging solforw and solrev and adding it to final time
    Y[1:2*q+1,:,:] = cat(solrev.y, solforw.y[2:end,:,:], dims = 1)
    # display(Y[q+1,:,:])

    # Get Acclerations from iteration 1 to 2*q+1;
    for i = 1:2*q+1
        Acc[i,:] = getA(Y[i,1,:])
    end

    # Getting Coefficients for order = order + 1
    γ,_ = AdamsBashforthCoef(order = order)
    γγ,_ = AdamsMoultonCoef(order = order+1)
    δ, δδ = StoemerCowellCoef(order = order+1)

    # 1st inverse del operated value at time = T[q+1] i.e. at initial state
    inv1∇_0 = Y[2*q+1,2,:]/h
    inv2∇_0 = Y[2*q+1,1,:]/(h^2)

    for j = 1:order
        inv1∇_0 -= γγ[j+1]*∇(j-1,2*q+1,Acc,zeros(1,m[2]),zeros(1,m[2]),q,dp,2*q+1)
    end

    for j = 1:order+1
        inv2∇_0 -= δδ[j+1]*∇(j-2,2*q+1,Acc,inv1∇_0,zeros(1,m[2]),q,dp,2*q+1)
    end

    # Change upper limit to n --- Testing phase
    for i = 2*q+2 : 1 : n
        # Predictor Formula - Position
        for j = 0 : 1 : order+1
            Y[i,1,:] += (h^2)*δ[j+1]*∇(j-2,i-1,Acc,inv1∇_0,inv2∇_0,q,dp,i-1)
        end

        # Predictor Formula - Velocity
        for j = 0 : 1 : order
            Y[i,2,:] += (h)*γ[j+1]*∇(j-1,i-1,Acc,inv1∇_0,inv2∇_0,q,dp,i-1)
        end

        # Getting Accleration from predicted value
        Acc[i,:] = getA(Y[i,1,:])

        # Reset the value of Y[i] to zeros
        Y[i,1,:] = zeros(1,m[2])
        Y[i,2,:] = zeros(1,m[2])

        # Corrector Formula -
        for j = 0 : 1 : order+1
            Y[i,1,:] += (h^2)*δδ[j+1]*∇(j-2,i,Acc,inv1∇_0,inv2∇_0,q,dp,i);
        end

        # Corrector Formula - Velocity
        for j = 0 : 1 : order
            Y[i,2,:] += (h)*γγ[j+1]*∇(j-1,i,Acc,inv1∇_0,inv2∇_0,q,dp,i)
        end

        # Getting Accleration from corrected value
        Acc[i,:] = getA(Y[i,1,:])
    end

    #Putting all data in States
    States = [State() for i in q+1:1:n]
    for i = q+1 : n
       States[i-q].acceleration.x = Acc[i,1]
       States[i-q].acceleration.y = Acc[i,2]
       States[i-q].position.x = Y[i,1,1]
       States[i-q].position.y = Y[i,1,2]
       States[i-q].velocity.x = Y[i,2,1]
       States[i-q].velocity.y = Y[i,2,2]
       States[i-q].time = T[i]
    end

    #Getting velocity as well using single integration
    function df2(y, t)
        # [vx vy ; ax ay]
        dz = zeros(size(y))
        dz[1,:] = y[2,:]
        dz[2,:] = getA(y[1,:])
        return dz
    end
    #Instead of this return states
    return States
end


"""
getStateByTime(;states :: Vector{State}, time :: Float64) :: State

Returns state at a particular time using Linear Interpolation.

    INPUT :
        Compulsory arguments
            states::Vector{State} - Vector containing all the States
            time::Float64 - Time for which State is needed.
                            Must be between intial and final value of time of States Vector

    OUTPUT :
        Returns retState::State - State variable corresponding to time variable
"""
function getStateByTime(;states :: Vector{State}, time :: Float64)::State
    numOfStates = size(states)[1]

    t0 = states[1].time
    tn = states[end].time

#     Checking time variable lies between t0 and tn
    if(time < t0 || time > tn)
        error("Provided time is out of bounds, should lie between t = ",t0, " and t = ", tn)
    else
        println("Provided time is under bounds")
        h = (tn - t0)/(numOfStates-1)
        @assert(h  > 0.0)

#       Floor
        index_lower = Int64(floor((time - t0)/h)) + 1
#       Ceil
        index_upper = Int64(ceil((time - t0)/h)) + 1

        if(index_lower == index_upper)
            return states[index_lower]
        end

#         Applying Linear Interpolation
#          (x-x1)*((y2 - y1)/(x2-x1) + y1 = y
        m = (states[index_upper] - states[index_lower])/(states[index_upper].time - states[index_lower].time)

        retState = (time - states[index_lower].time)*m + states[index_lower]
        return retState
    end
end
