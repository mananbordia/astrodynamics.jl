"""
RK4(df,yi,ti,h)

Predicts the value of function at next step using 4th Order Runge Kutta Method

    INPUT :
        Compulsory arguments :
            df - Function df(t)/dt
            yi - Matrix containing the intital values of function
            t - Initial time
            h - Time step

    OUTPUT :
        Returns a Matrix ϕ - Contains the values of function at next step
"""
function RK4(df,yi,ti,h)
    k1 = h*df(yi, ti)
    k2 = h*df(yi + 0.5*k1, ti + 0.5*h)
    k3 = h*df(yi + 0.5*k2, ti + 0.5*h)
    k4 = h*df(yi + k3, ti + h)
    return ϕ = yi + (k1 + 2*k2 + 2*k3 + k4)/6.0
end
