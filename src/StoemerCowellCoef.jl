#### Coefficient Generator for StoemerCowell - Predictor and Corrector -- Requires -> $order$ -- Default values -> $order$ = 8
"""
StoemerCowellCoef(;order=8)

Generates Stoemer Cowell Coefficients for a particular order

    INPUT :
        Optional Argument :
            order - order of the method (default : 8)
    OUTPUT :
        γ - Coefficients for Stoemer Cowell Predictor formula
        β - Coefficients for Stoemer Cowell Corrector formula

"""
function StoemerCowellCoef(;order=8)
    γ,tmp = AdamsMoultonCoef(order = order)
    δ = zeros(order+1)
    for j = 0:order
        δ[j+1] = (1-j)*γ[j+1]
    end
#   δδ = δ* corresponding to Corrector Formula
    δδ = zeros(order+1)
    δδ[1] = δ[1]
    for j = 1:order
        δδ[j+1] = δ[j+1] - δ[j]
    end
    return δ, δδ
end
