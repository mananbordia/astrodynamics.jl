function getReduced(;matr, newSize = 100)
    n,m = size(matr)
    mod = div(m,newSize)
    reduc = []
    for i = 1 : n
        reduc = cat(dims = 1,reduc,[matr[i,findall(x->x%mod==0, collect(1:m))];matr[i,end]]')
        @assert(reduc[i,end]==matr[i,end])
    end
    return reduc
end

function orbit_plotter(;States, key)
    p = Plots.plot()
    if 1 in key
        PeR, _ = getPeriMatrices(States = States)
        p = Plots.plot!(PeR[1,:],PeR[2,:], PeR[3,:], label = "Perifocal Frame")
    end

    if 2 in key
        InR, _ = getInertialMatrices(States = States)
        p = Plots.plot!(InR[1,:], InR[2,:], InR[3,:], label = "Inertial Frame")
    end

    if 3 in key
        EcR, _ = getEarthCenteredMatrices(States = States)
        p = Plots.plot!(EcR[1,:], EcR[2,:], EcR[3,:], label = "Earth Frame")
    end
    display(p)
end

function orbit_plotter3d(;States, R = 6400km, key, steps = 100)
    println("Be patient this might take few mins")
    spn = 180
    spu = LinRange(0,2*π,spn)
    spv = LinRange(0,π,spn)

    spx = R*cos.(spu) * sin.(spv)'
    spy = R*sin.(spu) * sin.(spv)'
    spz = R*ones.(spn) * cos.(spv)'

    scene = Scene()
    Makie.surface!(scene,spx,spy,spz)

    if 1 in key
        PeR = getReduced(matr = getPeriMatrices(States = States)[1], newSize = steps)
        Makie.lines!(scene,PeR[1,:]/1.0, PeR[2,:]/1.0, PeR[3,:]/1.0)
    end

    if 2 in key
        InR = getReduced(matr = getInertialMatrices(States = States)[1],newSize = steps)
        Makie.lines!(scene,InR[1,:]/1.0, InR[2,:]/1.0, InR[3,:]/1.0)
    end

    if 3 in key
        EcR = getReduced(matr = getEarthCenteredMatrices(States = States)[1],newSize = steps)
        Makie.lines!(scene,EcR[1,:]/1.0, EcR[2,:]/1.0, EcR[3,:]/1.0)
    end

#     Unusual behavior : Dividing by 1.0 required
#     Makie.lines!(scene,EcR[1,:],EcR[2,:],EcR[3,:])

# issue : Makie doesn't close in Mac
    display(scene)
end
