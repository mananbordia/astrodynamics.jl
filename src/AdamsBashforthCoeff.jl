#### Coefficient Generator for AdamsBashforth - Predictor and Corrector -- Requires -> $order$ -- Default values -> $order$ = 8  
"""
AdamsBashforthCoef(; order = 8)

Generates Adams Bashforth Coefficients for a particular order

    INPUT :
        Optional Argument :
            order - order of the method (default : 8)

    OUTPUT :
        γ - Coefficients for Adams Bashforth Predictor formula
        β - Coefficients for Adams Bashforth Corrector formula

"""
function AdamsBashforthCoef(; order = 8)
    # Index starts from 1 instead of 0 i.e b[i] means b[i-1];
    γ = ones(order+1);
    for i = 1:order
        for j = 0:i-1
            γ[i+1] -= γ[j+1]/(i-j+1)
        end
    end

    β = zeros(order+1)
    for j = 1:order
        for l = order-j : 1 : order-1
            β[j+1] += γ[l+1]*binomialCoef(l,order-j)
        end
        β[j+1] *= (-1)^(order-j)
    end
    return γ,β
end
