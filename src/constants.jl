"""
cTime helps to converts time duration to seconds
"""
module cTime
    sec = 1
    min = 60
    hour = min*60
    day = hour*24
    week = 7*day
    year = day*365
end

"""
cDist helps to converts any distance to meter
"""
module cDist
    m = 1
    km = 1000m
    cm = 0.01m
    mm = 0.1cm
    ft =  0.3048m
    inch = 25.4 * mm
end
