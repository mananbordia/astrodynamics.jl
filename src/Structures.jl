# Author : Manan Bordia
# Last Update : 4 December 2020

"""
    KelperElements contains the 6 keplerian element (e, ω, Ω, a, I, M)
# Examples
```julia-repl
julia> kepele = KeplerElements(0.2, 1.1, 0.8, 42000*cDist.km, 0.1, 0)
KeplerElements(0.2, 1.1, 0.8, 4.2e7, 0.1, 0.0)

julia> print(kepele.ω)
1.1

julia> kelpele2 = KeplerElements()
KeplerElements(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
```
"""
mutable struct KeplerElements
    e :: Float64
    ω :: Float64
    Ω :: Float64
    a :: Float64
    I :: Float64
    M :: Float64
    KeplerElements() = new(0,0,0,0,0,0)
    KeplerElements(e,ω,Ω,a,I,M) = new(e,ω,Ω,a,I,M)
end

"""
    Velocity contains velocity in x, y, z direction (x, y, z)
# Examples
```julia-repl
julia> V = Astrodynamics.Velocity(1000, -2333, 0)
Astrodynamics.Velocity(1000.0, -2333.0, 0.0)

julia> print((V.x, V.y, V.z))
(1000.0, -2333.0, 0.0)

julia> V2 = Astrodynamics.Velocity()
Astrodynamics.Velocity(0.0, 0.0, 0.0)
```
"""
mutable struct Velocity
    x :: Float64
    y :: Float64
    z :: Float64
    Velocity() = new(0,0,0)
    Velocity(x,y,z) = new(x,y,z)
end

"""
    Postion contains position in x, y, z direction (x, y, z)
# Examples
```julia-repl
julia> R = Astrodynamics.Position()
Astrodynamics.Position(0.0, 0.0, 0.0)

julia> R2 = Astrodynamics.Position(1*cDist.km, 2*cDist.km, 4.2*cDist.km)
Astrodynamics.Position(1000.0, 2000.0, 4200.0)

julia> print((R2.x, R2.y, R2.z))
(1000.0, 2000.0, 4200.0)
```
Note : We have used `cDist` for converting kilometer to meter.
"""
mutable struct Position
    x :: Float64
    y :: Float64
    z :: Float64
    Position() = new(0,0,0)
    Position(x,y,z) = new(x,y,z)
end

"""
    Accleration contains accleration in x, y, z direction (x, y, z)
# Examples
```julia-repl
julia> a = Astrodynamics.Acceleration(1,2,3)
Astrodynamics.Acceleration(1.0, 2.0, 3.0)

julia> print(a.x," ",a.y," ",a.z)
1.0 2.0 3.0

julia> a = Astrodynamics.Acceleration()
Astrodynamics.Acceleration(0.0, 0.0, 0.0)
```
"""
mutable struct Acceleration
    x :: Float64
    y :: Float64
    z :: Float64
    Acceleration() = new(0,0,0)
    Acceleration(x,y,z) = new(x,y,z)
end

## State is the main structure and it contains - velocity, position and acceleration as subvariables
"""
    State is the main structure and it contains - velocity, position and acceleration as subvariables
```julia-repl
julia> state = State()
State(Astrodynamics.Velocity(0.0, 0.0, 0.0), Astrodynamics.Position(0.0, 0.0, 0.0), Astrodynamics.Acceleration(0.0, 0.0, 0.0), -1.0)

julia> state2 = State(Astrodynamics.Velocity(), Astrodynamics.Position(), Astrodynamics.Acceleration(),2*cTime.hour)
State(Astrodynamics.Velocity(0.0, 0.0, 0.0), Astrodynamics.Position(0.0, 0.0, 0.0), Astrodynamics.Acceleration(0.0, 0.0, 0.0), 7200.0)
```
"""
mutable struct State
    velocity :: Velocity
    position :: Position
    acceleration :: Acceleration
    time :: Float64
    State() = new(Velocity(), Position(),Acceleration(),-1)
    State(v,p,a,t) = new(v,p,a,t)
end


mutable struct Perifocal
    x :: Float64
    y :: Float64
    z :: Float64
    Perifocal() = new(0,0,0)
    Perifocal(x,y,z) = new(x,y,z)
end

mutable struct Inertial
    x :: Float64
    y :: Float64
    z :: Float64
    Inertial() = new(0,0,0)
    Inertial(x,y,z) = new(x,y,z)
end

mutable struct Earth
    x :: Float64
    y :: Float64
    z :: Float64
    Earth() = new(0,0,0)
    Earth(x,y,z) = new(0,0,0)
end

mutable struct FVelocity
    perifocal :: Perifocal
    inertial :: Inertial
    earth :: Earth
    FVelocity() = new(Perifocal(),Inertial(),Earth())
    FVelocity(a,b,c) = new(a,b,c)
end

mutable struct FPosition
    perifocal :: Perifocal
    inertial :: Inertial
    earth :: Earth
    FPosition() = new(Perifocal(),Inertial(),Earth())
    FPosition(a,b,c) = new(a,b,c)
end

mutable struct FState
    velocity :: FVelocity
    position :: FPosition
    FState() = new(FVelocity(), FPosition())
    FState(a,b) = new(a, b)
end


# Tested : Working

# Operations for Position
Base.:+(a::Position, b::Position) = Position(a.x + b.x, a.y + b.y, a.z + b.z)
Base.:-(a::Position, b::Position) = Position(a.x - b.x, a.y - b.y, a.z - b.z)
Base.:*(a::Position, b::Number) = Position(a.x * b, a.y * b, a.z * b)
Base.:*(b::Number,a::Position) = Position(a.x * b, a.y * b, a.z * b)
Base.:/(a::Position, b::Number) = Position(a.x / b, a.y / b, a.z / b)

# Operations for Velocity
Base.:+(a::Velocity, b::Velocity) = Velocity(a.x + b.x, a.y + b.y, a.z + b.z)
Base.:-(a::Velocity, b::Velocity) = Velocity(a.x - b.x, a.y - b.y, a.z - b.z)
Base.:*(a::Velocity, b::Number) = Velocity(a.x * b, a.y * b, a.z * b)
Base.:*(b::Number,a::Velocity) = Velocity(a.x * b, a.y * b, a.z * b)
Base.:/(a::Velocity, b::Number) = Velocity(a.x / b, a.y / b, a.z / b)

# Operations for Acceleration
Base.:+(a::Acceleration, b::Acceleration) = Acceleration(a.x + b.x, a.y + b.y, a.z + b.z)
Base.:-(a::Acceleration, b::Acceleration) = Acceleration(a.x - b.x, a.y - b.y, a.z - b.z)
Base.:*(a::Acceleration, b::Number) = Acceleration(a.x * b, a.y * b, a.z * b)
Base.:*(b::Number,a::Acceleration) = Acceleration(a.x * b, a.y * b, a.z * b)
Base.:/(a::Acceleration, b::Number) = Acceleration(a.x / b, a.y / b, a.z / b)

# Operations for State
Base.:-(a::State, b::State) = State(a.velocity - b.velocity, a.position - b.position, a.acceleration - b.acceleration, a.time - b.time)
Base.:+(a::State, b::State) = State(a.velocity + b.velocity, a.position + b.position, a.acceleration + b.acceleration, a.time + b.time)
Base.:*(a::State, b::Number) = State(a.velocity * b, a.position * b, a.acceleration*b, a.time * b)
Base.:*(b::Number, a::State) = State(a.velocity * b, a.position * b, a.acceleration*b, a.time * b)
Base.:/(a::State, b::Number) = State(a.velocity / b, a.position / b, a.acceleration/b, a.time / b)
