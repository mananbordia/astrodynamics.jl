#### Binomial Coefficients $\sigma \choose j $ -- Requires -> $\sigma$ and $j$

"""
binomialCoef(σ,j)

This function returns σ choose j

    INPUT :
        Compulsory arguments :
            σ - Integer
            j - Non negative integer

    OUTPUT :
        Binomial Coefficient - Double

# Examples
```julia-repl
julia> Astrodynamics.binomialCoef(10,2)
45.0

julia> Astrodynamics.binomialCoef(-10,2)
55.0

julia> Astrodynamics.binomialCoef(-1.3,2)
1.4949999999999999
```
"""
function binomialCoef(σ,j)
    # σ < 0 and 0<= j < m
    if(j==0)
        return 1
    end
    numerator = 1
    for i = σ : -1 : σ - j +1
        numerator *= i
    end
    denominator = 1
    for i = 1 : 1 : j
        denominator *= i
    end
    binom = numerator/denominator
    return binom
end
