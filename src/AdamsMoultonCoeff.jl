#### Coefficient Generator for AdamMoulton - Predictor and Corrector -- Requires -> $order$ -- Default values -> $order$ = 8
"""
AdamsMoultonCoef(; order = 8)

Generates Adams Moulton Coefficients for a particular order

    INPUT :
        Optional Argument :
            order - order of the method (default : 8)

    OUTPUT :
        γ - Coefficients for Adams Moulton Predictor formula
        β - Coefficients for Adams Moulton Corrector formula

"""
function AdamsMoultonCoef(; order = 8)
    # Index starts from 1 instead of 0 i.e b[i] means b[i-1];
    # γγ == γ⋆
    γγ = zeros(order+1);
    γγ[1] = 1
    for i = 1:order
        for j = 0:i-1
            γγ[i+1] -= γγ[j+1]/(i-j+1)
        end
    end

    ββ = zeros(order+1)
    for j = 1:order
        for l = order-j : 1 : order-1
            ββ[j+1] += γγ[l+1]*binomialCoef(l,order-j)
        end
        ββ[j+1] *= (-1)^(order-j)
    end
    return γγ,ββ
end
