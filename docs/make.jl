push!(LOAD_PATH,"..")
using Astrodynamics
using Documenter

DocMeta.setdocmeta!(Astrodynamics, :DocTestSetup, :(using Astrodynamics); recursive=true)

makedocs(
    modules=[Astrodynamics],
    authors="mananbordia <mananbordia@iitk.ac.in> and contributors",
    repo="https://gitlab.com/mananbordia/astrodynamics.jl/blob/{commit}{path}#{line}",
    sitename="Astrodynamics.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://mananbordia.gitlab.io/astrodynamics.jl/dev",
        assets=String[],
    ),
    pages=[
        "Introduction" => "index.md",
        "Tools" => "tools.md",
    ],
)

deploydocs(
    repo = "gitlab.com/mananbordia/astrodynamics.jl.git",
)
