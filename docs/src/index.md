# Introduction to Astrodynamics

`Astrodynamics.jl` is a package that can help you in dealing with Two body problems, Predicting and Visualising satellite orbit path under Kelperian Law.


## Installing Package

You can obtain Astrodynamics using Julia's Pkg REPL-mode (hitting `]` as the first character of the command prompt):

```julia
(@v1.6) pkg> add "https://gitlab.com/mananbordia/astrodynamics.jl.git"
```

or with `using Pkg; Pkg.add(url = "https://gitlab.com/mananbordia/astrodynamics.jl.git")`


You can also check the version of package installed:

```julia
(@v1.6) pkg> status Astrodynamics
# Output
Status `~/.julia/environments/v1.6/Project.toml`
  [ad27efbb] Astrodynamics v0.1.0 `https://gitlab.com/mananbordia/astrodynamics.jl.git#master`
```

## Importing Package Tools

```julia
julia> using Astrodynamics
```

That's all you need to do. Now you are ready to use the tools provided by package.


## What Astrodynamics can do

Astrodynamics can .............
- .......
- .......
- .......
- .......
- .......


## What else do I need to know?

.............................


## Uninstalling Package

You can remove Astrodynamics using Julia's Pkg REPL-mode (hitting `]` as the first character of the command prompt):

```julia
(@v1.6) pkg> rm Astrodynamics
```

or with `using Pkg; Pkg.rm("Astrodynamics")`


## If Astrodynamics doesn't work as expected

Astrodynamics is still in beta phase and there are lot more things that need to be added.
So if something isn't working for you or incase you find any bugs in the documentation, please [file an issue](https://gitlab.com/mananbordia/astrodynamics.jl/-/issues).
